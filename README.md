# rcube_proxy roundcube plugin

rcube_proxy makes is possible to switch between imap servers based on a database entry. If database contains the login username the plugin switches to proxy_host over ssl otherwise connects to the local mailbox. The plugin is part of a larger migration project but maybe can be helpful for others.

Entries below in config.inc.php are required.
In the original project proxy table contains the column 'username' which is a must for the current functionality.

$config['db_dsn_vimbadmin'] = 'mysql://_username_:_password_@localhost/vimbadmin';
$config['vimbadmin_proxy_table'] = 'proxy';

$config['proxy_host'] = 'circlemail.co.uk';
$config['proxy_port'] = 993;
$config['proxy_ssl'] = 'ssl';
$config['imap_host'] = 'circlemail.co.uk';
