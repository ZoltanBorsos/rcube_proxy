<?php

/*************************************************
* Login requests are being proxied based on a    *
* database lookup.				 *
*						 *
* 						 *
*************************************************/

class rcube_proxy extends rcube_plugin
{

    function init()
    {
        $this->add_hook('startup', array($this, 'startup'));
    }

    function startup($args)
    {

	$this->load_config();

	$rcmail = rcmail::get_instance();

	// connect to Vimbadmin database
	$db = rcube_db::factory($rcmail->config->get('db_dsn_vimbadmin'));

	$db->set_debug($rcmail->config->get('sql_debug'));
	$db->db_connect('r'); // connect in read mode

	// retrieve table name
        $proxy_table = $rcmail->config->get('vimbadmin_proxy_table');

        $sql_result = $db->query('SELECT * FROM ' . $proxy_table
                .' WHERE `username` = ?', $_SESSION['username']); // ? is replaced with emailaddress

	$sql_array = $db->fetch_assoc($sql_result);

	$proxy = 0;
	if ( $sql_array != NULL  ){
	  $proxy = 1;
        }


	if ( $proxy == 1 ) {
	  $_SESSION['storage_host'] = $rcmail->config->get('proxy_host');
	  $_SESSION['storage_port'] = $rcmail->config->get('proxy_port');
	  $_SESSION['storage_ssl'] = $rcmail->config->get('proxy_ssl');
	  $_SESSION['imap_host'] = $rcmail->config->get('imap_host');
	} else {
	  $_SESSION['storage_host'] = 'localhost';
	  $_SESSION['storage_port'] = 143;
	  $_SESSION['storage_ssl'] = '';
	  $_SESSION['imap_host'] = 'localhost';
	}

        // handle login action
        if (empty($_SESSION['user_id'])) {
            $args['action']         = 'login';
            $this->redirect_query   = $_SERVER['QUERY_STRING'];
        }
        // Set user password in session (see shutdown() method for more info)
        else if (!empty($_SESSION['user_id']) && empty($_SESSION['password'])
                 && !empty($_SERVER['PHP_AUTH_PW'])) {
            $_SESSION['password'] = $rcmail->encrypt($_SERVER['PHP_AUTH_PW']);
        }

        return $args;
    }

}
